<?php
/**
 * @file
 * Contains \Drupal\my_database\Controller\MyDatabaseController.
 */
namespace Drupal\my_database\Controller;
use Drupal\Core\Url;

/**
 * Class MyDatabaseController
 * @package Drupal\my_database\Controller
 */
class MyDatabaseController {
  /**
   * Function content()
   *
   * @return null
   */
  public function content() {
    $connection = \Drupal::database();
    $header = array(t('Id'), t('Number'), t('Teaser'), t('Text'), t('Update'), t('Delete'));
    $rows = array();
    $query = $connection->select('custom_table', 't')
      ->fields('t', array('id', 'number', 'teaser', 'text'))
      ->execute();
    while ($value = $query->fetchAssoc()) {
      $upd = Url::fromRoute('my_database_update', array('id' => $value['id']));
      $del = Url::fromRoute('my_database_delete', array('id' => $value['id']));
      $rows[] = array(
        $value['id'],
        $value['number'],
        $value['teaser'],
        $value['text'],
        \Drupal::l('Update', $upd),
        \Drupal::l('Delete', $del),
      );
    }
    $table = array(
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );

    $element = array(
      '#markup' => render($table),
    );
    return $element;
  }
}