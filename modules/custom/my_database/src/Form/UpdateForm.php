<?php
/**
 * @file
 * Contains \Drupal\my_database\Form\InsertForm.
 */

namespace Drupal\my_database\Form;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Unicode;
use Drupal\my_database\MyDatabaseBaseForm;

/**
 * Class UpdateForm
 * @package Drupal\my_database
 */
class UpdateForm extends MyDatabaseBaseForm {
  /**
   * Function getFormId()
   *
   * @return string
   */
  public function getFormId() {
    return 'my_database_update';
  }

  /**
   * Function database_update_title()
   */
  public function database_update_title() {
    $connection = \Drupal::database();
    $number = $connection->select('custom_table', 'n')
      ->fields('n', array('number'))
      ->condition('n.id', $this->GetId())
      ->execute()
      ->fetchField();
    if ($number)
      return t('Edit item with number "@number"', array('@number' => $number));
    else
      return t('ID not found.');
  }

  /**
   * Function buildForm()
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $cancel = Url::fromRoute('my_database_table');
    $connection = \Drupal::database();
    $query = $connection->select('custom_table', 'b')
      ->condition('id', $this->GetId())
      ->fields('b')
      ->execute()
      ->fetchAssoc();
    if (empty($query) || empty($form_state->getValue('number'))) {
      $form['text'] = array(
        '#markup' => t('Undefined "@id" id. @back.', array(
          '@id' => $this->GetId(),
          '@back' => \Drupal::l('Cancel', $cancel),
        ))
      );
      return $form;
    }
    $form['number'] = array(
      '#title' => t('Number'),
      '#type' => 'textfield',
      '#maxlength' => 10,
      '#required' => TRUE,
      '#default_value' => $query['number'],
    );
    $form['teaser'] = array(
      '#title' => t('Teaser'),
      '#type' => 'textfield',
      '#maxlength' => 150,
      '#required' => TRUE,
      '#default_value' => $query['teaser'],
    );
    $form['text'] = array(
      '#title' => t('Text'),
      '#type' => 'textarea',
      '#required' => TRUE,
      '#default_value' => $query['text'],
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );
    $form['actions']['cancel'] = array(
      '#markup' => \Drupal::l('Cancel', $cancel),
    );
    return $form;
  }

  /**
   * Function validateForm()
   *
   * @param array $form
   * @param array|FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_numeric($form_state->getValue('number'))) {
      $form_state->setErrorByName('number', $this->t('Field "@field" is not numeric.', array('@field' => t('Number'))));
    }
    if (Unicode::strlen($form_state->getValue('teaser')) > 150) {
      $form_state->setErrorByName('teaser', $this->t('Field "@field" must be less than @c chars.',
        array('@field' => t('Teaser'), '@c' => 150)));
    }
  }

  /**
   * Function submitForm()
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $number = $form_state->getValue('number');
    $teaser = $form_state->getValue('teaser');
    $text = $form_state->getValue('text');
    $connection = \Drupal::database();
    $connection->update('custom_table')
      ->fields(array(
        'number' => $number,
        'teaser' => $teaser,
        'text' => $text,
      ))
      ->condition('id', $this->GetId())
      ->execute();
    drupal_set_message(t('Successfully update!'));
    $form_state->setRedirect('my_database_table');
  }
}