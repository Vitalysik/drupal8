<?php
/**
 * @file
 * Contains \Drupal\my_database\Form\InsertForm.
 */

namespace Drupal\my_database\Form;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\my_database\MyDatabaseBaseForm;

/**
 * Class InsertForm
 * @package Drupal\my_database\Form
 */
class InsertForm extends MyDatabaseBaseForm {
  /**
   * Function getFormId()
   *
   * @return string
   */
  public function getFormId() {
    return 'my_database_insert';
  }

  /**
   * Function buildForm()
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['number'] = array(
      '#type' => 'textfield',
      '#title' => t('Number'),
      '#maxlength' => 10,
      '#required' => TRUE,
    );
    $form['teaser'] = array(
      '#type' => 'textfield',
      '#title' => t('Teaser'),
      '#maxlength' => 150,
      '#required' => TRUE,
    );
    $form['text'] = array(
      '#type' => 'textarea',
      '#title' => t('Text'),
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Insert'),
    );
    return $form;
  }

  /**
   * Function validateForm()
   *
   * @param array $form
   * @param array|FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_numeric($form_state->getValue('number'))) {
      $form_state->setErrorByName('number', $this->t('Field "@field" is not numeric.', array('@field' => t('Number'))));
    }
    if (Unicode::strlen($form_state->getValue('teaser')) > 150) {
      $form_state->setErrorByName('teaser', $this->t('Field "@field" must be less than @c chars.',
        array('@field' => t('Teaser'), '@c' => 150)));
    }
  }

  /**
   * Function submitForm()
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $connection = \Drupal::database();
    $number = $form_state->getValue('number');
    $teaser = $form_state->getValue('teaser');
    $text = $form_state->getValue('text');
    $connection->insert('custom_table')
      ->fields(array(
        'number' => $number,
        'teaser' => $teaser,
        'text' => $text,
      ))
      ->execute();
    drupal_set_message(t('Successfully added!'));
    $form_state->setRedirect('my_database_table');
  }
}