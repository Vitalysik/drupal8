<?php
/**
 * @file
 * Contains \Drupal\my_database\Form\InsertForm.
 */

namespace Drupal\my_database\Form;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\my_database\MyDatabaseBaseForm;

/**
 * Class DeleteForm
 * @package Drupal\my_database
 */
class DeleteForm extends MyDatabaseBaseForm {
  /**
   * Function getFormId()
   *
   * @return string
   */
  public function getFormId() {
    return 'my_database_delete';
  }

  /**
   * Function database_delete_title()
   */
  public function database_delete_title() {
    $connection = \Drupal::database();
    $number = $connection->select('custom_table', 'n')
      ->fields('n', array('number'))
      ->condition('n.id', $this->GetId())
      ->execute()
      ->fetchField();
    if ($number)
      return t('Are you sure you want to delete item with number "@number"?', array('@number' => $number));
    else
      return t('ID not found.');
  }

  /**
   * Function buildForm()
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $cancel = Url::fromRoute('my_database_table');
    $connection = \Drupal::database();
    $query = $connection->select('custom_table', 'b')
      ->condition('id', $this->GetId())
      ->fields('b')
      ->execute()
      ->fetchAssoc();
    if (empty($query)) {
      $form['text'] = array(
        '#markup' => t('Undefined "@id" id. @back.', array(
          '@id' => $this->GetId(),
          '@back' => \Drupal::l('Cancel', $cancel),
        ))
      );
      return $form;
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
    $form['cancel'] = array(
      '#markup' => \Drupal::l('Cancel', $cancel),
    );
    return $form;
  }

  /**
   * Function submitForm()
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $connection = \Drupal::database();
    $connection->delete('custom_table')
      ->condition('id', $this->GetId())
      ->execute();
    drupal_set_message(t('Successfully delete!'));
    $form_state->setRedirect('my_database_table');
  }
}