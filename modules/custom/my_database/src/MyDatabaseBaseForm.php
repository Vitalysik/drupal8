<?php
/**
 * @file
 * Contains \Drupal\my_database\MyDatabaseBaseForm.
 */

namespace Drupal\my_database;
use Drupal\Core\Form\FormBase;

/**
 * Class MyDatabaseBaseForm
 * @package Drupal\my_database
 */
abstract class MyDatabaseBaseForm extends FormBase {
  /**
   * function GetId()
   */
  static function GetId() {
    $current_path_id = \Drupal::service('path.current')->getPath();
    $path_args_id = explode('/', $current_path_id);
    $id = $path_args_id[3];
    return $id;
  }
}