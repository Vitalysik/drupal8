<?php
/**
 * @file
 * Contains \Drupal\my_form\Form\ContactsForm.
 */

namespace Drupal\my_form\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContactsForm
 * @package Drupal\my_form\Form
 */
class ContactsForm extends FormBase {
  /**
   * Function getFormId()
   *
   * @return string
   */
  public function getFormId() {
    return 'contacts_form';
  }

  /**
   * Function buildForm()
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#maxlength' => 60,
    );
    $form['surname'] = array(
      '#type' => 'textfield',
      '#title' => t('Surname'),
      '#maxlength' => 60,
    );
    $form['nickname'] = array(
      '#type' => 'textfield',
      '#title' => t('Nickname'),
      '#maxlength' => 60,
      '#required' => TRUE,
    );
    $form['email'] = array(
      '#type' => 'email',
      '#title' => t('Email'),
      '#required' => TRUE,
    );
    $form['phone'] = array(
      '#type' => 'tel',
      '#title' => t('Telephone'),
    );
    $form['sex'] = array(
      '#type' => 'radios',
      '#title' => t('Sex'),
      '#options' => array(t('Male'), t('Female')),
    );
    $form['country'] = array(
      '#type' => 'select',
      '#title' => t('Country'),
      '#options' => array(t('Ukraine'), t('Russia'), t('USA')),
    );
    $form['message'] = array(
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#required' => TRUE,
    );
    $form['agree'] = array(
      '#type' => 'checkbox',
      '#title' => t('I agree'),
      '#required' => TRUE,
    );
    $form['mes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Click'),
      '#description' => t('Hello World!'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Sent'),
    );
    return $form;
  }

  /**
   * Function validateForm()
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('phone') and !is_numeric($form_state->getValue('phone'))) {
      $form_state->setErrorByName('phone',
        $this->t("The phone '%phone' is not numeric.", array('%phone' => $form_state->getValue('phone'))));
    }
  }

  /**
   * Function submitForm()
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sex = $form['sex']['#options'][$form_state->getValue('sex')];
    $cr = $form['country']['#options'][$form_state->getValue('country')];
    $title = t('Result:');
    $type = 'ul';
    $items = array(
        t('Name: @name', array('@name' => $form_state->getValue('name'))),
        t('Surname: @surname', array('@surname' => $form_state->getValue('surname'))),
        t('Nickname: @nickname', array('@nickname' => $form_state->getValue('nickname'))),
        t('E-mail: @email', array('@email' => $form_state->getValue('email'))),
        t('Telephone: @phone', array('@phone' => $form_state->getValue('phone'))),
        t('Sex: @sex', array('@sex' => $sex)),
        t('Country: @contry', array('@contry' => $cr)),
        t('Message: @message', array('@message' => $form_state->getValue('message'))),
    );
    $list = array(
      '#theme' => 'item_list',
      '#title' => $title,
      '#list_type' => $type,
      '#items' => $items,
    );
    drupal_set_message(render($list));
  }
}
?>

